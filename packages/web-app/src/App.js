import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';

const fetchBooks = async () => {
  const res = await fetch('/books')
  const data = await res.json()

  return data
}

function App() {
  const [books, setBooks] = useState([])
  const [error, setError] = useState(undefined)

  console.log('books', books)

  useEffect(() => {
    fetchBooks()
      .then((data) => setBooks(data))
      .catch(e => setError(error?.message || 'Something unexpected happened'))
  }, [])

  if (error) {
    return (<div>Error: {error}</div>)
  }

  const bookItems = books.map(b => <li key={b.title}>{b.title}</li>)

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

      <div>
        <h1>Books</h1>
        <ul>
          {bookItems}
        </ul>
      </div>
    </div>
  );
}

export default App;

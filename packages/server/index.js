const express = require('express')
const app = express()
const port = process.env.PORT || 3000

const BOOKS = [
    { title: 'Book 1' },
    { title: 'Book 2' },
    { title: 'Book 3' },
]

app.get('/books', (req, res) => {
    res.status(200).json(BOOKS)
})

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})